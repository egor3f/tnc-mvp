FROM node:16 AS builder

WORKDIR /app
COPY package.json ./
RUN yarn install
COPY . .
RUN yarn run build

FROM nginx

COPY --from=builder /app/dist /usr/share/nginx/html
COPY nginx_default.conf /etc/nginx/conf.d/default.conf
