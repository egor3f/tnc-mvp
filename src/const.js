export const LIGHT = 'light';
export const DARK = 'dark';

export const CHART_TEXT_COLOR = {
  [LIGHT]: '#000',
  [DARK]: '#fff'
}
export const CHART_EXTRA_COLOR = {
  [LIGHT]: '#f00',
  [DARK]: '#f00'
}

export const INTERVALS = [
  {name: '1 минута',  value: '1m',   intvalue: 60},
  {name: '3 минуты',  value: '3m',   intvalue: 180},
  {name: '5 минут',   value: '5m',   intvalue: 300},
  {name: '15 минут',  value: '15m',  intvalue: 900},
  {name: '30 минут',  value: '30m',  intvalue: 1800},
  {name: '1 час',     value: '1h',   intvalue: 3600},
  {name: '2 часа',    value: '2h',   intvalue: 3600 * 2},
  {name: '4 часа',    value: '4h',   intvalue: 3600 * 4},
  {name: '6 часов',   value: '6h',   intvalue: 3600 * 6},
  {name: '8 часов',   value: '8h',   intvalue: 3600 * 8},
  {name: '12 часов',  value: '12h',  intvalue: 3600 * 12},
  {name: '1 день',    value: '1d',   intvalue: 3600 * 24},
  {name: '3 дня',     value: '3d',   intvalue: 3600 * 24 * 3},
  {name: '1 неделя',  value: '1w',   intvalue: 3600 * 24 * 7},
  {name: '1 месяц',   value: '1M',   intvalue: 3600 * 24 * 30},
]

export const NEWS_MODE_HOVER = 'hover';
export const NEWS_MODE_FIXED = 'fixed';
export const NEWS_MODE_HIDE = 'hide';
