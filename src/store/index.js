import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import {INTERVALS, LIGHT, NEWS_MODE_FIXED, NEWS_MODE_HIDE, NEWS_MODE_HOVER} from "@/const";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    quote: '',
    theme: LIGHT,
    interval: INTERVALS[0],
    accounts: [],

    newsMode: NEWS_MODE_HIDE,
    prevNewsMode: NEWS_MODE_HIDE,
    newsList: [],
    newsListTime: null, // Последняя кликнутая свечка
  },
  mutations: {
    setQuote(store, ticker) {
      store.quote = ticker;
    },
    setTheme(store, theme) {
      store.theme = theme;
      document.documentElement.setAttribute('theme', theme);
    },
    setInterval(store, interval) {
      store.interval = interval;
    },
    setAccounts(store, accs) {
      store.accounts = accs;
    },

    setNewsList(store, newsList) {
      store.newsList = newsList;
    },
    setNewsMode(store, newsMode) {
      if(store.newsMode !== newsMode) store.prevNewsMode = store.newsMode;
      store.newsMode = newsMode;
      console.log(`Set news mode: ${newsMode}`);
    },
    setNewsModeFixed(store, candleTime) { // Срабатывает по клику по свечке
      if(store.newsListTime === candleTime) // Повторный клик возвращает предыдущий режим
        this.commit('setNewsMode', store.prevNewsMode);
      else
        this.commit('setNewsMode', NEWS_MODE_FIXED);
      store.newsListTime = candleTime;
    },
    toggleNewsMode(store) { // Срабатывает по пробелу
      if(store.newsMode === NEWS_MODE_HOVER && store.newsList.length)
        this.commit('setNewsMode', NEWS_MODE_HIDE);
      else
        this.commit('setNewsMode', NEWS_MODE_HOVER);
      store.newsListTime = null;
    },
    hideNews(store) {
      this.commit('setNewsMode', NEWS_MODE_HIDE);
      store.newsListTime = null;
    }
  },
  actions: {
  },
  modules: {
  },
  plugins: [
    createPersistedState({
      paths: ['quote', 'theme', 'interval', 'accounts']
    }),
  ],
})
