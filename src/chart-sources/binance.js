import Vue from "vue";
import store from '../store';

const API_ROOT = 'https://api.binance.com/api/v3';

export async function getSymbolsList() {
  let response = await Vue.http.get(`${API_ROOT}/exchangeInfo`);
  let body = await response.json();
  return body['symbols'].map(symbol => symbol['symbol']);
}

export async function loadKlines(startTime=null, endTime=null) {
  if(!store.state.quote || !store.state.interval) return [];
  let params = {
    symbol: store.state.quote,
    interval: store.state.interval.value,
    // limit: 500,
  };
  if(startTime) params['startTime'] = parseInt(startTime);
  if(endTime) params['endTime'] = parseInt(endTime);
  console.log(`Loading klines: params: ${JSON.stringify(params)}`);
  let repsonse = await Vue.http.get(`${API_ROOT}/klines`, {params});
  let body = await repsonse.json();
  return body.map(candle => candle.map(value => parseFloat(value)).slice(0, 6));
}
