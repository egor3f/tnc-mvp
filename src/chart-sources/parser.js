import Vue from "vue";
import store from '../store';

const API_ROOT = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : '/api';

export async function loadEvents(startTime=null, endTime=null) {
  let params = {};
  if(startTime) params['since'] = parseInt(startTime);
  if(endTime) params['till'] = parseInt(endTime);
  if(store.state.accounts.length) {
    params['accounts'] = store.state.accounts;
  }
  console.debug(`Loading events: params: ${JSON.stringify(params)}`);
  try {
    let response = await Vue.http.get(`${API_ROOT}/events{?accounts*}`, {params});
    let events = await response.json();
    return events.map(event => [event['timestamp'] * 1000, event]);
  } catch (e) {
    console.error(e);
    return [];
  }
}

export async function loadSources() {
  try {
    let response = await Vue.http.get(`${API_ROOT}/sources`);
    return await response.json();
  } catch (e) {
    console.error(e);
    return [];
  }
}
