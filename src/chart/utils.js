import {format} from "date-fns";

export function printDates(caption, ...dates) {
  console.log(`${caption.padEnd(10, ' ')} ` + dates.map(arg => format(new Date(arg), 'dd.MM.yyyy HH:mm')).join('\t'));
}
